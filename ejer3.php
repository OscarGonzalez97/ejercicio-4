<?php
$dbconn = pg_connect("host=localhost port=5432 user=postgres password=postgres dbname=ejercicio1")
or die("No e ha podido conectar");

$result = pg_query($dbconn, "SELECT producto.nombre, precio, marca.nombre as nombre_marca, empresa.nombre as nombre_empresa,
categoria.nombre as categoria_nombre
FROM producto
inner join marca ON marca.id_marca = producto.id_marca
inner join empresa ON empresa.id_empresa = marca.id_empresa
inner join categoria ON categoria.id_categoria = producto.id_categoria;");
echo "<table>";
echo "<th>";
echo "<td>Nom. Producto</td>";
echo "<td>Precio</td>";
echo "<td>Marca</td>";
echo "<td>Empresa</td>";
echo "<td>Categoria</td>";
echo "</th>";
while($row = pg_fetch_row($result)) {
    echo "<tr>";
    echo "<td>$row[0]</td>";
    echo "<td>$row[1]</td>";
    echo "<td>$row[2]</td>";
    echo "<td>$row[3]</td>";
    echo "<td>$row[4]</td>";
    echo "</tr>";
}
echo "</table";
pg_close($dbconn);
